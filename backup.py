"""
https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask
https://flask-restful.readthedocs.io/en/latest/quickstart.html#endpoints
https://programminghistorian.org/en/lessons/creating-apis-with-python-and-flask
http://nightdeveloper.net/construir-flask-python/
https://realpython.com/flask-connexion-rest-api/
https://www.pyimagesearch.com/2018/05/21/an-opencv-barcode-and-qr-code-scanner-with-zbar/
https://www.youtube.com/watch?v=-4MPtERPq2E

"""


from flask import Flask, jsonify , request, render_template
import requests

app = Flask(__name__,template_folder='templates')

serverName = 'http://localhost:8888/'
serverPort = '8888'

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol', 
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web', 
        'done': False
    }
]


"""
Backend
	APIS para guardar data
	/authorization/login    POST
	/user/{userId}          GET
	/interaction/           POST
	/interaction/           GET

"""


@app.route('/authorization/login', methods=['POST'])
def routeLogin():
    #values = request.get_json()
    result = {
        'code': '000',
        'message': 'Successful'
    }
    return jsonify({'result': result})

@app.route('/user', methods=['GET'])
def routeGetUserInformation():

    result = {
        'id_user': 1000 ,
        'name': 'Royer Leandro',
        'lastname': 'Robles',
        'motherlastname': 'Vega',
        'username': 'royer.robles',
        'password': 'royer.robles',
        'area': 'IT'
    }

    return jsonify({'userInformation': result})

@app.route('/users', methods=['GET'])
def routeListUsers():

    result = {
        'id_user': 1000 ,
        'name': 'Royer Leandro',
        'lastname': 'Robles',
        'motherlastname': 'Vega',
        'username': 'royer.robles',
        'password': 'royer.robles',
        'area': 'IT'
    }

    return jsonify({'userInformation': result})

@app.route('/user-status', methods=['POST'])
def updateStatusUsers():
    requestBody = request.get_json()
    """
    example = {
        'userDanger': 'U1000' ,
        'usersDanger': ['U1001','U1002']
    }
    """
    return jsonify({'userInformation': result})

@app.route('/interaction-register', methods=['POST'])
def routeSaveInteractions():
    values = request.get_json()
    return jsonify({'values': values})

@app.route('/interactions-by-user', methods=['GET'])
def routeGetInteractions():

    mockInteracciones = [
            {
                "userName":"raul.penilla",
                "date":"17052020",
                "time":"20:00 PM"
            },{
                "userName":"gloria.paredes",
                "date":"17052020",
                "time":"20:00 PM"
            },{
                "userName":"saul.urgente",
                "date":"17052020",
                "time":"20:00 PM"
            }
        ]

    return jsonify({'interactions': mockInteracciones})


@app.route('/')
def index():
   return render_template('index.html')

@app.route('/offcanvas/index')
def listado():
   res = requests.get('http://localhost:8888/interaction')
   print(res.json())
   #data = res.json()
   return render_template('offcanvas/index.html')

@app.route('/album/index')
def album():
   #res = requests.get('http://localhost:8888/interaction')
   #print(res.json())
   #data = res.json()
   return render_template('album/index.html')


if __name__ == "__main__":
    app.run(debug=True,port=8888)